#!/usr/bin/python3
import subprocess
import bbvapi
import config
import os
import click
import time
import json
import signal
import psutil
import tempfile
from halo import Halo

spinner = Halo(spinner="dots")


def check_if_interface_up():
    # TODO: extend this
    return os.path.exists(f"/sys/class/net/{config.interface_name}")


def del_ip_with_default_route(ip, check=False):
    default_route = subprocess.run(
        "ip r | grep default | head -n 1", capture_output=True, shell=True
    ).stdout.decode()
    subprocess.run(
        f"ip r del {default_route.replace('default', ip)}",
        shell=True,
        check=check,
        capture_output=True,
    )


def add_ip_with_default_route(ip, check=False):
    default_route = subprocess.run(
        "ip r | grep default | head -n 1", capture_output=True, shell=True
    ).stdout.decode()
    subprocess.run(
        f"ip r add {default_route.replace('default', ip)}",
        shell=True,
        check=check,
        capture_output=True,
    )


def start_and_pid(command, pid_path, stop_old=True):
    stop_pid(pid_path)

    cmd = subprocess.Popen(
        command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
    )
    with open(pid_path, "w") as f:
        f.write(str(cmd.pid))
    return cmd


def _get_pid_from_pidfile(pid_path):
    if not os.path.exists(pid_path):
        return False

    with open(pid_path) as f:
        pid = f.read()

    if not psutil.pid_exists(int(pid)):
        return False

    return int(pid)


def check_pid_running(pid_path):
    pid = _get_pid_from_pidfile(pid_path)
    return bool(pid)


def stop_pid(pid_path):
    pid = _get_pid_from_pidfile(pid_path)
    if pid:
        os.kill(pid, signal.SIGKILL)
        os.unlink(pid_path)


@click.group(invoke_without_command=True)
@click.pass_context
def cli(ctx):
    return


@cli.command(help="Spin up and connect to VPN in a region.")
@click.option(
    "--region",
    default="DE",
    help="Name of region (run `region` command for full list).",
)
@click.option(
    "--deploytype",
    default="wireguard",
    help="Deployment type.",
)
def up(region, deploytype):
    if check_if_interface_up():
        return click.echo("You're already connected to the VPN.")

    spinner.start("Checking if there's any running servers...")
    host_state = bbvapi.get_state()
    spinner.stop_and_persist(symbol="✅", text="Checked server status.")
    if not host_state["running"]:
        spinner.start("Spinning up a server...")
        bbvapi.spin_up(region, deploytype)
        spinner.stop_and_persist(symbol="✅", text=f"Spun up server.")
    if not host_state["running"] or not host_state.get("conf_ready", False):
        spinner.start("Waiting for server to get ready...")
        while True:
            host_state = bbvapi.get_state()
            if host_state.get("conf_ready", False):
                break
            time.sleep(3)
        spinner.stop_and_persist(symbol="✅", text="Server is ready.")

    spinner.start("Getting wireguard config...")
    conf = bbvapi.get_conf().replace("[CLIENT PRIVKEY GOES HERE]", config.wg_privkey)
    spinner.stop_and_persist(symbol="✅", text="Got wireguard config.")

    with open(f"/etc/wireguard/{config.interface_name}.conf", "w") as f:
        f.write(conf)

    if deploytype == "wireguardshadowsocks":
        spinner.start("Setting up shadowsocks...")
        shadowsocks_conf_path = os.path.join(tempfile.gettempdir(), "shadowsocks.json")
        with open(shadowsocks_conf_path, "w") as f:
            json.dump(
                {
                    "local_address": "127.0.0.1",
                    "local_port": 6969,
                    "server": host_state["ipv4"],
                    "server_port": config.shadowsocks_port,
                    "password": host_state["server_secret"],
                    "method": "aes-256-gcm",
                    "mode": "udp_only",
                },
                f,
            )

        pid_path = os.path.join(tempfile.gettempdir(), "bbvshadowsocks.pid")
        start_and_pid(
            [
                config.sslocal_path,
                "-c",
                shadowsocks_conf_path,
                "-f",
                f"127.0.0.1:{config.wireguard_port}",
                "--protocol",
                "tunnel",
            ],
            pid_path,
        )

        add_ip_with_default_route(host_state["ipv4"])
        spinner.stop_and_persist(symbol="✅", text="Shadowsocks started.")

    spinner.start("Connecting to VPN...")
    subprocess.run(
        f"wg-quick up {config.interface_name}", shell=True, capture_output=True
    )
    spinner.stop_and_persist(
        symbol="✅",
        text=f"Connected. Your traffic is now routed through {host_state['ipv4']}.",
    )


@cli.command(help="Disconnect and delete VPN server.")
def down():
    spinner.start("Checking and stopping child processes...")

    pid_path = os.path.join(tempfile.gettempdir(), "bbvshadowsocks.pid")
    stop_pid(pid_path)

    spinner.stop_and_persist(symbol="✅", text="Stopped child processes.")

    if check_if_interface_up():
        spinner.start("Disconnecting from VPN...")
        subprocess.run(
            f"wg-quick down {config.interface_name}", shell=True, capture_output=True
        )
        spinner.stop_and_persist(symbol="✅", text="Disconnected.")

    config_path = f"/etc/wireguard/{config.interface_name}.conf"
    if os.path.exists(config_path):
        os.unlink(config_path)

    host_state = bbvapi.get_state()
    del_ip_with_default_route(host_state["ipv4"])
    if host_state["running"]:
        spinner.start("Spinning down server...")
        bbvapi.spin_down()
        spinner.stop_and_persist(symbol="✅", text="Spun down server.")


@cli.command(help="Get a list of available regions.")
def regions():
    regions = bbvapi.get_regions()
    print("- " + ("\n- ".join(regions)))


if __name__ == "__main__":
    cli()
