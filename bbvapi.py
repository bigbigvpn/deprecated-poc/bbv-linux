import requests
import config


def get_regions():
    req = requests.get(
        config.bbvhost + "/api/regions",
        headers={"Authorization": "Bearer " + config.apikey},
    )
    assert req.status_code == 200
    return req.json()["countries"]


def get_state():
    req = requests.get(
        config.bbvhost + "/api/server",
        headers={"Authorization": "Bearer " + config.apikey},
    )
    assert req.status_code == 200
    return req.json()


def spin_up(region, deploytype):
    req = requests.post(
        config.bbvhost + "/api/server",
        json={
            "wg_pubkey": config.wg_pubkey,
            "country": region,
            "deploy_type": deploytype,
        },
        headers={"Authorization": "Bearer " + config.apikey},
    )
    assert req.status_code == 200
    return req.json()


def spin_down():
    req = requests.delete(
        config.bbvhost + "/api/server",
        headers={"Authorization": "Bearer " + config.apikey},
    )
    assert req.status_code == 200
    return req.json()


def get_conf():
    req = requests.get(
        config.bbvhost + "/api/server/conf",
        headers={"Authorization": "Bearer " + config.apikey},
    )
    assert req.status_code == 200
    return req.text
